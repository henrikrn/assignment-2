/**
 */
package henrik.studyPlan.tests;

import henrik.studyPlan.StudyPlanFactory;
import henrik.studyPlan.StudyProgram;

import junit.framework.TestCase;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Study Program</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are tested:
 * <ul>
 *   <li>{@link henrik.studyPlan.StudyProgram#getNoOfSemesters() <em>No Of Semesters</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public class StudyProgramTest extends TestCase {

	/**
	 * The fixture for this Study Program test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected StudyProgram fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(StudyProgramTest.class);
	}

	/**
	 * Constructs a new Study Program test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StudyProgramTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Study Program test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(StudyProgram fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Study Program test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected StudyProgram getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(StudyPlanFactory.eINSTANCE.createStudyProgram());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

	/**
	 * Tests the '{@link henrik.studyPlan.StudyProgram#getNoOfSemesters() <em>No Of Semesters</em>}' feature getter.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see henrik.studyPlan.StudyProgram#getNoOfSemesters()
	 * @generated NOT
	 */
	public void testGetNoOfSemesters() {
		StudyProgram sp = StudyPlanFactory.eINSTANCE.createStudyProgram();
		sp.setYears(2);
		assertEquals(sp.getNoOfSemesters(), 4);
	}

} //StudyProgramTest
