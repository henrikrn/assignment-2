/**
 */
package henrik.studyPlan.tests;

import henrik.studyPlan.Course;
import henrik.studyPlan.Department;
import henrik.studyPlan.StudyPlanFactory;
import henrik.studyPlan.StudyProgram;
import junit.framework.TestCase;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Department</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are tested:
 * <ul>
 *   <li>{@link henrik.studyPlan.Department#getNoOfCourses() <em>No Of Courses</em>}</li>
 *   <li>{@link henrik.studyPlan.Department#getCode() <em>Code</em>}</li>
 *   <li>{@link henrik.studyPlan.Department#getTitle() <em>Title</em>}</li>
 * </ul>
 * </p>
 * <p>
 * The following operations are tested:
 * <ul>
 *   <li>{@link henrik.studyPlan.Department#addCourse(henrik.studyPlan.Course) <em>Add Course</em>}</li>
 *   <li>{@link henrik.studyPlan.Department#addStudyProgram(henrik.studyPlan.StudyProgram) <em>Add Study Program</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public class DepartmentTest extends TestCase {

	/**
	 * The fixture for this Department test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Department fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(DepartmentTest.class);
	}

	/**
	 * Constructs a new Department test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DepartmentTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Department test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(Department fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Department test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Department getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(StudyPlanFactory.eINSTANCE.createDepartment());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

	/**
	 * Tests the '{@link henrik.studyPlan.Department#getNoOfCourses() <em>No Of Courses</em>}' feature getter.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see henrik.studyPlan.Department#getNoOfCourses()
	 * @generated NOT
	 */
	public void testGetNoOfCourses() {
		Department department = StudyPlanFactory.eINSTANCE.createDepartment();
		for (int i = 0; i < 3; i++) {
			Course course = StudyPlanFactory.eINSTANCE.createCourse();
			department.addCourse(course);
		}
		assertEquals(department.getNoOfCourses(), 3);
	}

	/**
	 * Tests the '{@link henrik.studyPlan.Department#getCode() <em>Code</em>}' feature getter.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see henrik.studyPlan.Department#getCode()
	 * @generated NOT
	 */
	public void testGetCode() {
		Department department = StudyPlanFactory.eINSTANCE.createDepartment();
		String code = "Department of Computer Science";
		department.setName(code + " - Department of Computer Science");
		assertEquals(department.getCode(), code);
	}

	/**
	 * Tests the '{@link henrik.studyPlan.Department#getTitle() <em>Title</em>}' feature getter.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see henrik.studyPlan.Department#getTitle()
	 * @generated NOT
	 */
	public void testGetTitle() {
		Department department = StudyPlanFactory.eINSTANCE.createDepartment();
		String title = "Department of Computer Science";
		department.setName("IDI - " + title);
		assertEquals(department.getTitle(), title);
	}

	/**
	 * Tests the '{@link henrik.studyPlan.Department#addCourse(henrik.studyPlan.Course) <em>Add Course</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see henrik.studyPlan.Department#addCourse(henrik.studyPlan.Course)
	 * @generated NOT
	 */
	public void testAddCourse__Course() {
		Department department = StudyPlanFactory.eINSTANCE.createDepartment();
		Course course = StudyPlanFactory.eINSTANCE.createCourse();
		department.addCourse(course);
		assertEquals(department.getCourses().get(0), course);
	}

	/**
	 * Tests the '{@link henrik.studyPlan.Department#addStudyProgram(henrik.studyPlan.StudyProgram) <em>Add Study Program</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see henrik.studyPlan.Department#addStudyProgram(henrik.studyPlan.StudyProgram)
	 * @generated NOT
	 */
	public void testAddStudyProgram__StudyProgram() {
		Department department = StudyPlanFactory.eINSTANCE.createDepartment();
		StudyProgram sp = StudyPlanFactory.eINSTANCE.createStudyProgram();
		department.addStudyProgram(sp);
		assertEquals(department.getStudyPrograms().get(0) , sp);
	}

} //DepartmentTest
