/**
 */
package henrik.studyPlan.tests;

import henrik.studyPlan.Course;
import henrik.studyPlan.Semester;
import henrik.studyPlan.StudyPlanFactory;

import junit.framework.TestCase;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Semester</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following operations are tested:
 * <ul>
 *   <li>{@link henrik.studyPlan.Semester#addMandatoryCourse(henrik.studyPlan.Course) <em>Add Mandatory Course</em>}</li>
 *   <li>{@link henrik.studyPlan.Semester#addElectiveCourse(henrik.studyPlan.Course) <em>Add Elective Course</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public class SemesterTest extends TestCase {

	/**
	 * The fixture for this Semester test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Semester fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(SemesterTest.class);
	}

	/**
	 * Constructs a new Semester test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SemesterTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Semester test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(Semester fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Semester test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Semester getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(StudyPlanFactory.eINSTANCE.createSemester());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

	/**
	 * Tests the '{@link henrik.studyPlan.Semester#addMandatoryCourse(henrik.studyPlan.Course) <em>Add Mandatory Course</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see henrik.studyPlan.Semester#addMandatoryCourse(henrik.studyPlan.Course)
	 * @generated NOT
	 */
	public void testAddMandatoryCourse__Course() {
		Semester semester = StudyPlanFactory.eINSTANCE.createSemester();
		Course course = StudyPlanFactory.eINSTANCE.createCourse();
		semester.addMandatoryCourse(course);
		assertEquals(semester.getMandatoryCourses().get(0), course);
	}

	/**
	 * Tests the '{@link henrik.studyPlan.Semester#addElectiveCourse(henrik.studyPlan.Course) <em>Add Elective Course</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see henrik.studyPlan.Semester#addElectiveCourse(henrik.studyPlan.Course)
	 * @generated NOT
	 */
	public void testAddElectiveCourse__Course() {
		Semester semester = StudyPlanFactory.eINSTANCE.createSemester();
		Course course = StudyPlanFactory.eINSTANCE.createCourse();
		semester.addElectiveCourse(course);
		assertEquals(semester.getElectiveCourses().get(0), course);
	}

} //SemesterTest
