# Assignment 2

### How to run M2T transformation
1. Go to: henrik.sp.acceleo > src > henrik > sp > acceleo > main
2. Right click 'generate.mtl' > Run configurations
3. Choose studyPlan.xmi when browsing at the 'model' field
4. Select target directory (should already be set to be under the current 'main' folder)
5. Run the transormation

### Notes about the instance model
The studyPlan.xmi model contains the IDI department, which contains a study program (MIDT). This study program has 4 specializations, which each contains 2 semesters (Fall and Spring). Rather than having 'status' as in the study program pages I chose to have a column named 'Obligatory'. Each semester also contains 4 courses.

### Changes from assignment 1
* Added derived features for 'title' and 'code' for the Department.
* Added 'years' attribute in StudyProgram which is how many years the program is (2 years/3 years/5 years)
    * 'noOfSemesters' is now a derived feature based on 'years'
* Added tests for manually written code (see henrik.sp.model.tests)
