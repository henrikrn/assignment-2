/**
 */
package henrik.studyPlan;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Course</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link henrik.studyPlan.Course#getName <em>Name</em>}</li>
 *   <li>{@link henrik.studyPlan.Course#getCode <em>Code</em>}</li>
 *   <li>{@link henrik.studyPlan.Course#getStudyPoints <em>Study Points</em>}</li>
 *   <li>{@link henrik.studyPlan.Course#getOwnedBy <em>Owned By</em>}</li>
 * </ul>
 *
 * @see henrik.studyPlan.StudyPlanPackage#getCourse()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='validCode'"
 * @generated
 */
public interface Course extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see henrik.studyPlan.StudyPlanPackage#getCourse_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link henrik.studyPlan.Course#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Code</em>' attribute.
	 * @see #setCode(String)
	 * @see henrik.studyPlan.StudyPlanPackage#getCourse_Code()
	 * @model dataType="henrik.studyPlan.CourseCode"
	 * @generated
	 */
	String getCode();

	/**
	 * Sets the value of the '{@link henrik.studyPlan.Course#getCode <em>Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Code</em>' attribute.
	 * @see #getCode()
	 * @generated
	 */
	void setCode(String value);

	/**
	 * Returns the value of the '<em><b>Study Points</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.Float}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Study Points</em>' attribute list.
	 * @see henrik.studyPlan.StudyPlanPackage#getCourse_StudyPoints()
	 * @model upper="30"
	 * @generated
	 */
	EList<Float> getStudyPoints();

	/**
	 * Returns the value of the '<em><b>Owned By</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link henrik.studyPlan.Department#getCourses <em>Courses</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Owned By</em>' container reference.
	 * @see #setOwnedBy(Department)
	 * @see henrik.studyPlan.StudyPlanPackage#getCourse_OwnedBy()
	 * @see henrik.studyPlan.Department#getCourses
	 * @model opposite="courses" required="true" transient="false"
	 * @generated
	 */
	Department getOwnedBy();

	/**
	 * Sets the value of the '{@link henrik.studyPlan.Course#getOwnedBy <em>Owned By</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Owned By</em>' container reference.
	 * @see #getOwnedBy()
	 * @generated
	 */
	void setOwnedBy(Department value);

} // Course
