/**
 */
package henrik.studyPlan;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Specialization</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see henrik.studyPlan.StudyPlanPackage#getSpecialization()
 * @model
 * @generated
 */
public enum Specialization implements Enumerator {
	/**
	 * The '<em><b>Software systems</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SOFTWARE_SYSTEMS_VALUE
	 * @generated
	 * @ordered
	 */
	SOFTWARE_SYSTEMS(0, "Software_systems", "Software_systems"),

	/**
	 * The '<em><b>Databases and search</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DATABASES_AND_SEARCH_VALUE
	 * @generated
	 * @ordered
	 */
	DATABASES_AND_SEARCH(1, "Databases_and_search", "Databases_and_search"),

	/**
	 * The '<em><b>Artificial intelligence</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ARTIFICIAL_INTELLIGENCE_VALUE
	 * @generated
	 * @ordered
	 */
	ARTIFICIAL_INTELLIGENCE(2, "Artificial_intelligence", "Artificial_intelligence"),

	/**
	 * The '<em><b>Algorithms and computers</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ALGORITHMS_AND_COMPUTERS_VALUE
	 * @generated
	 * @ordered
	 */
	ALGORITHMS_AND_COMPUTERS(3, "Algorithms_and_computers", "Algorithms_and_computers");

	/**
	 * The '<em><b>Software systems</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SOFTWARE_SYSTEMS
	 * @model name="Software_systems"
	 * @generated
	 * @ordered
	 */
	public static final int SOFTWARE_SYSTEMS_VALUE = 0;

	/**
	 * The '<em><b>Databases and search</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DATABASES_AND_SEARCH
	 * @model name="Databases_and_search"
	 * @generated
	 * @ordered
	 */
	public static final int DATABASES_AND_SEARCH_VALUE = 1;

	/**
	 * The '<em><b>Artificial intelligence</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ARTIFICIAL_INTELLIGENCE
	 * @model name="Artificial_intelligence"
	 * @generated
	 * @ordered
	 */
	public static final int ARTIFICIAL_INTELLIGENCE_VALUE = 2;

	/**
	 * The '<em><b>Algorithms and computers</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ALGORITHMS_AND_COMPUTERS
	 * @model name="Algorithms_and_computers"
	 * @generated
	 * @ordered
	 */
	public static final int ALGORITHMS_AND_COMPUTERS_VALUE = 3;

	/**
	 * An array of all the '<em><b>Specialization</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final Specialization[] VALUES_ARRAY =
		new Specialization[] {
			SOFTWARE_SYSTEMS,
			DATABASES_AND_SEARCH,
			ARTIFICIAL_INTELLIGENCE,
			ALGORITHMS_AND_COMPUTERS,
		};

	/**
	 * A public read-only list of all the '<em><b>Specialization</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<Specialization> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Specialization</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param literal the literal.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static Specialization get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			Specialization result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Specialization</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name the name.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static Specialization getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			Specialization result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Specialization</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the integer value.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static Specialization get(int value) {
		switch (value) {
			case SOFTWARE_SYSTEMS_VALUE: return SOFTWARE_SYSTEMS;
			case DATABASES_AND_SEARCH_VALUE: return DATABASES_AND_SEARCH;
			case ARTIFICIAL_INTELLIGENCE_VALUE: return ARTIFICIAL_INTELLIGENCE;
			case ALGORITHMS_AND_COMPUTERS_VALUE: return ALGORITHMS_AND_COMPUTERS;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private Specialization(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //Specialization
